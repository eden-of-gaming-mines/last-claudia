from urllib.request import urlopen, Request, HTTPError
import io
import os

ROOT = os.path.dirname(os.path.realpath(__file__))
ASSET_DIR = os.path.join(ROOT, 'assets')
os.makedirs(ASSET_DIR, exist_ok=True)

STANDARD = 1550222
def main():
    assets = parse_manifestheader(download_asset("manifestheader.bin.bytes"))
    for asset, ahash in assets.items():
        fp = os.path.join(ASSET_DIR, asset)
        if os.path.exists(fp):
            continue
        print(asset)
        try:
            data = download_asset(asset)
        except HTTPError:
            print('Failed to download', asset)
            continue
        with open(fp, 'wb') as f:
            f.write(data)


PROD_HEADERS = {
    "X-Unity-Version": "2018.4.7f1",
    "Token": "KoynH9vG",
    "User-Agent": "Dalvik/2.1.0 (Linux; U; Android 5.1.1; oppo r9tm Build/LYZ28N)",
    "Host": "prod-res.world.aidis-games.com",
    "Connection": "Keep-Alive",
    "Accept-Encoding": "gzip"
}

def download_asset(name, white = True):
    url = f"https://prod-res.world.aidis-games.com/white/assetBundle/Standard{STANDARD}/Android/{name}"
    #for url in ["/white/assetBundle/Master1580193/Android/mst"]:
    print(url)
    req = Request(url, headers=PROD_HEADERS)
    return urlopen(req).read()

def parse_manifestheader(data : bytes) -> dict:
    """
    experimental parser
    :input data: manifest header (binary)
    :returns: dict with asset_name : hash pairs
    """
    import re

    return {
        match[1].decode('utf8') : match[2]   
        for match in re.finditer(b'(\w+?\d*?)\x00([\w\d]{32})', data)
    }

def weirdo_login():
    # probably not needed?
    """
    POST https://prod-app.world.aidis-games.com/gme/actionSymbol/aQJ1fA9S.php HTTP/1.1
    Expect: 100-continue
    X-Unity-Version: 2018.4.7f1
    Content-type: application/x-msgpack
    Content-Length: 696
    User-Agent: Dalvik/2.1.0 (Linux; U; Android 5.1.1; oppo r9tm Build/LYZ28N)
    Host: prod-app.world.aidis-games.com
    Connection: Keep-Alive
    Accept-Encoding: gzip
    """
    base64_body = "g6ZUX0JPRFmBpVBfS0VZxQI9g6g1RXV6cW8xY4KmUF9LRVlT3AARqDRUeFdNMFFEqGVUbmo5M3diqFBjczk1ZHBTqHd1eVEzcW43qHVWNVRtMHNoqEQzR2Q2UEtDqEw2eFNKVmE1qHZLRUdMSjI5qFZMYWgyOGZTqGJuRzZMMklwqG8wNE5IZXNNqGRyeDF6VTc5qGdlMHZCeDVXqEoxQlhMRnQ5qHF4MTR6SlRvqE00ZTU5Q3dBqFlqMTBXUUFYqFBfVkFMVUVTkdwAEakxNzk0Mzg3MzWjLS0toTKub3BwbyBvcHBvIHI5dG2nMTAwMjAwOdk8OTQwZWU0MTItMGNmMi00ZmU1LTg1MjktYTdhYmIzZmViYWViLjhkNzUzMDI2NTU5MzJmMi5BbmRyb2lk2TNBbmRyb2lkIE9TIDUuMS4xIC8gQVBJLTIyIChMWVoyOE4vVjkuNS4yLjAuTEFDQ05GQSnZJDllNDViMWJiLTc0ZjctNDIwNS04NjlmLWM0OWMwODhkMWJjZqCpMTE1MTUzMjk2oTKoUXVhbGNvbW2tT3BlbkdMIEVTIDMuMa9BZHJlbm8gKFRNKSA1NDCgoKCoM1hrc3cyWVOCplBfS0VZU5OoNERwNVZMc2ioM250aUtVZDKoOFJwVDRVTEqoUF9WQUxVRVOSk7FGX1ZFUlNJT05fQU5EUk9JRKdBbmRyb2lkpzEwMDIwMDmTqHI0TFc2ZGpLwKEwqDJ5dmcwOEdLgqZQX0tFWVORpVBfS0VZqFBfVkFMVUVTkZGgqFRfSEVBREVSxFOCplBfS0VZU5OockhkcGc5ejSoc1c0QThuVGSoS2lwMTlZZGuoUF9WQUxVRVORk7BwdTI1WDhtbzNXZjZDNE45oTGwYjloMmZyWlhNNm5kSkIwealUX01FU1NBR0XA"

if __name__ == '__main__':
    main()